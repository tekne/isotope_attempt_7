use super::Set;

/// The type of 8-bit bytes
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub struct Byte;
impl Set for Byte {}

/// The type of strings
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub struct Str;
impl Set for Str {}

/// An enum of primitive isotope types
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum Primitive {
    Byte,
    Str
}
impl From<Byte> for Primitive {
    fn from(_ : Byte) -> Self { Primitive::Byte }
}
impl From<Str> for Primitive {
    fn from(_ : Str) -> Self { Primitive::Str }
}
impl Set for Primitive {}

/// The empty type. It's values cannot be constructed (as there are none)
pub struct Empty;
impl Set for Empty {}
