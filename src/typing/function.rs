use owning_ref::OwningRef;
use stable_deref_trait::StableDeref;

use std::convert::TryInto;
use std::sync::Arc;

use crate::value::Value;
use super::{Context, Type, Universe};

//TODO: this
#[derive(Debug, Clone)]
pub struct FunctionTypeError {}

/// The trait implemented by all isotope function types which can be interpreted
/// in a given context. Not object-safe.
pub trait FunctionType<C: Context> : Type<C> where Self : Sized {
    /// The argument type of this function
    type ArgumentType : Type<C>;
    /// The result type of this function
    type ResultType : Type<C> + ?Sized;
    /// A (potentially smart) reference to the result type of this function
    type ResultRef : AsRef<Self::ResultType>;
    /// Gets the number of arguments this function takes
    fn no_args(&self) -> usize;
    /// Get the type of this function when applied to a list of arguments,
    /// given as an iterator, with the guarantee that the application is partial
    fn partially_applied<'a, T, VS, V>(&'a self, v : VS, ctx : C)
    -> Result<Self, FunctionTypeError> where
        VS: Iterator<Item=&'a V>,
        V: 'a + Value<C, T>,
        T: Type<C> + TryInto<Self::ArgumentType>;
    /// Get the type of this function when applied to a list of arguments,
    /// given as an iterator, with the guarantee that the application is complete
    fn totally_applied<'a, T, VS, V>(&'a self, v : VS, ctx : C)
    -> Result<Self::ResultRef, FunctionTypeError> where
        VS: Iterator<Item=&'a V>,
        V: 'a + Value<C, T>,
        T: Type<C> + TryInto<Self::ArgumentType>;
    /// Get the type of this function when applied to a list of arguments,
    /// given as an iterator.
    fn applied<'a, T, VS, V>(&'a self, v : VS, ctx : C)
    -> Result<FunctionOrResult<Self, Self::ResultRef>, FunctionTypeError> where
        VS: Iterator<Item=&'a V>,
        V: 'a + Value<C, T>,
        T: Type<C> + TryInto<Self::ArgumentType>;
}

impl<C: Context> FunctionType<C> for ! {
    type ArgumentType = !;
    type ResultType = !;
    type ResultRef = Box<!>; // Lil' hack to get the AsRef<!>
    fn no_args(&self) -> usize { *self }
    fn partially_applied<'a, T, VS, V>(&'a self, _v : VS, _ctx : C)
    -> Result<Self, FunctionTypeError> where
        VS: Iterator<Item=&'a V>,
        V: 'a + Value<C, T>,
        T: Type<C> + TryInto<Self::ArgumentType> { *self }
    fn totally_applied<'a, T, VS, V>(&'a self, _v : VS, _ctx : C)
    -> Result<Self::ResultRef, FunctionTypeError> where
        VS: Iterator<Item=&'a V>,
        V: 'a + Value<C, T>,
        T: Type<C> + TryInto<Self::ArgumentType> { *self }
    fn applied<'a, T, VS, V>(&'a self, _v : VS, _ctx : C)
    -> Result<FunctionOrResult<Self, Self::ResultRef>, FunctionTypeError> where
        VS: Iterator<Item=&'a V>,
        V: 'a + Value<C, T>,
        T: Type<C> + TryInto<Self::ArgumentType> { *self }
}

//TODO: dependent functions!
pub struct Function<A, R = A, AO = Arc<[A]>, RO = Arc<R>> {
    arguments : OwningRef<AO, [A]>,
    result : OwningRef<RO, R>
}

pub enum FunctionOrResult<F, R> {
    Function(F),
    Result(R)
}

impl<A, R, AO, RO> Function<A, R, AO, RO> {
    #[inline]
    pub fn new<AT: Into<OwningRef<AO, [A]>>, RT: Into<OwningRef<RO, R>>>(args : AT, res : RT)
    -> Self {
        Function { arguments : args.into(), result : res.into() }
    }
    /// Get the result type of this function when applied to all of its arguments
    #[inline] pub fn result(&self) -> &R { self.result.as_ref() }
    /// Get a slice of the argument types of this function, in order
    #[inline] pub fn args(&self) -> &[A] { self.arguments.as_ref() }
}


impl<A, R, AO, RO, C> Type<C> for Function<A, R, AO, RO> where
    C: Context + Clone,
    A: Type<C>,
    R: Type<C> {
    /// The universe-level of this type, i.e. the universe containing it
    fn level(&self, ctx : C) -> Universe {
        let arg_level = self.arguments.iter()
            .map(|arg| arg.level(ctx.clone()))
            .max().unwrap_or(Universe::set());
        std::cmp::max(arg_level, self.result.level(ctx))
    }
    /// Whether this type is a kind, i.e. holds types
    #[inline(always)] fn is_kind(&self, _ctx : C) -> bool { false }
}

impl<A, R, AO, RO> Function<A, R, AO, RO> where
    AO: StableDeref,
    OwningRef<AO, [A]>: Clone,
    OwningRef<RO, R>: Clone {
    fn count_args<'a, C, T, VS, V>(&'a self, v : VS, ctx : C) -> Result<usize, FunctionTypeError>
        where
        C: Context + Clone,
        A: Type<C>,
        R: Type<C>,
        VS: Iterator<Item=&'a V>,
        V: 'a + Value<C, T>,
        //TODO: think about this...
        T: Type<C> + TryInto<A> {
        let mut args = self.args().iter();
        let mut args_in_call : usize = 0;
        for val in v {
            match val.get_type(ctx.clone()).try_into() {
                Ok(_ty) => {
                    match args.next() {
                        Some(_aty) => {
                            //TODO: check argument types actually match
                            args_in_call += 1;
                        },
                        None => {
                            //TODO: this
                            return Err(FunctionTypeError {});
                        }
                    }
                },
                Err(_e) => {
                    //TODO: this
                    return Err(FunctionTypeError {});
                }
            }
        }
        Ok(args_in_call)
    }
}

impl<C, A, R, AO, RO> FunctionType<C> for Function<A, R, AO, RO> where
    C: Context + Clone,
    A: Type<C>,
    R: Type<C>,
    AO: StableDeref,
    OwningRef<AO, [A]>: Clone,
    OwningRef<RO, R>: Clone {
    type ArgumentType = A;
    type ResultType = R;
    type ResultRef = OwningRef<RO, R>;

    #[inline] fn no_args(&self) -> usize { self.args().len() }

    fn partially_applied<'a, T, VS, V>(&'a self, v : VS, ctx : C)
    -> Result<Self, FunctionTypeError> where
        VS: Iterator<Item=&'a V>,
        V: 'a + Value<C, T>,
        T: Type<C> + TryInto<Self::ArgumentType> {
        let args_in_call = self.count_args(v, ctx)?;
        let no_args = self.no_args();
        if args_in_call == no_args {
            panic!("This function call was asserted as partially applied")
        } else if args_in_call > no_args {
            panic!("args_in_call should never be greater than the number of arguments")
        } else {
            Ok(Function {
                arguments : self.arguments.clone().map(|args| &args[args_in_call..]),
                result : self.result.clone()
            })
        }
    }

    fn totally_applied<'a, T, VS, V>(&'a self, v : VS, ctx : C)
    -> Result<Self::ResultRef, FunctionTypeError> where
        VS: Iterator<Item=&'a V>,
        V: 'a + Value<C, T>,
        T: Type<C> + TryInto<Self::ArgumentType> {
        let args_in_call = self.count_args(v, ctx)?;
        let no_args = self.no_args();
        if args_in_call < no_args {
            panic!("This function call was asserted as totally applied")
        } else if args_in_call > no_args {
            panic!("args_in_call should never be greater than the number of arguments")
        } else {
            Ok(self.result.clone())
        }
    }

    fn applied<'a, T, VS, V>(&'a self, v : VS, ctx : C)
    -> Result<FunctionOrResult<Self, Self::ResultRef>, FunctionTypeError> where
        VS: Iterator<Item=&'a V>,
        V: 'a + Value<C, T>,
        T: Type<C> + TryInto<Self::ArgumentType> {
        let args_in_call = self.count_args(v, ctx)?;
        Ok(if args_in_call == self.no_args() {
            FunctionOrResult::Function(Function {
                arguments : self.arguments.clone().map(|args| &args[args_in_call..]),
                result : self.result.clone()
            })
        } else if args_in_call < self.no_args() {
            FunctionOrResult::Result(self.result.clone())
        } else {
            panic!("args_in_call should never be greater than the number of arguments!")
        })
    }
}
