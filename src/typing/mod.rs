use std::num::NonZeroUsize;

pub mod primitive;
pub mod function;

/// A typing universe
#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd)]
pub struct Universe(NonZeroUsize);

impl Universe {
    /// The universe of simple values
    #[inline(always)]
    pub fn set() -> Universe { Universe(NonZeroUsize::new(1).unwrap()) }
    /// The universe containing this universe
    #[inline(always)]
    pub fn next(self) -> Universe { Universe(NonZeroUsize::new(self.0.get() + 1).unwrap()) }
}

/// The trait implemented by all isotope contexts
pub trait Context {}

/// A context which is a subset of another
pub trait Subcontext<C: Context>: Context {
    fn from_ctx(ctx : C) -> Self;
}

/// The static isotope context, of which all other contexts are subcontexts
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub struct StaticCtx;

impl Context for StaticCtx {}
impl<C: Context> Subcontext<C> for StaticCtx { fn from_ctx(_ctx : C) -> Self { StaticCtx } }

/// The trait implemented by isotope types requiring a fixed typing context.
/// Automatically implements `Type`
pub trait ConType {
    /// The context needed to interpret this type
    type Context : Context;
    /// The universe-level of this type, i.e. the universe containing it
    fn level_c(&self, ctx : Self::Context) -> Universe;
    /// Whether this type is a kind, i.e. holds types
    fn is_kind_c(&self, ctx : Self::Context) -> bool;
}

/// A trait for simple types with no higher structure, needing no typing context.
/// Automatically implements `ConType` and hence `Type`
pub trait Set {}

impl<S: Set> ConType for S {
    type Context = StaticCtx;
    fn level_c(&self, _ctx : Self::Context) -> Universe { Universe::set() }
    fn is_kind_c(&self, _ctx : Self::Context) -> bool { false }
}

/// The trait implemented by all isotope types which can be interpreted in a given context.
/// You usually want to implement `ConType` or `Set` instead.
pub trait Type<C> {
    /// The universe-level of this type, i.e. the universe containing it
    fn level(&self, ctx : C) -> Universe;
    /// Whether this type is a kind, i.e. holds types
    fn is_kind(&self, ctx : C) -> bool;
}

impl<C, T> Type<C> for T where
    C: Context,
    T: ConType,
    T::Context : Subcontext<C> {
    #[inline] fn level(&self, ctx : C) -> Universe { self.level_c(T::Context::from_ctx(ctx)) }
    #[inline] fn is_kind(&self, ctx : C) -> bool { self.is_kind_c(T::Context::from_ctx(ctx)) }
}

impl<C> Type<C> for ! where C: Context {
    fn level(&self, _ctx : C) -> Universe { *self }
    fn is_kind(&self, _ctx : C) -> bool { *self }
}
