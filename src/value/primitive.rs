use std::borrow::Cow;

use super::ConValue;
use crate::typing::{
    StaticCtx,
    primitive::{Primitive as PrimitiveType, Byte, Str}
};

impl ConValue for u8 {
    type TypeKind = Byte;
    type Context = StaticCtx;
    fn get_type_c(&self, _ctx : Self::Context) -> Self::TypeKind { Byte }
}

impl ConValue for str {
    type TypeKind = Str;
    type Context = StaticCtx;
    fn get_type_c(&self, _ctx : Self::Context) -> Self::TypeKind { Str }
}

pub enum Primitive<'vcx> {
    Byte(u8),
    Str(Cow<'vcx, str>)
}

impl<'vcx> From<u8> for Primitive<'vcx> {
    fn from(b : u8) -> Self { Primitive::Byte(b) }
}

impl<'vcx> From<&'vcx str> for Primitive<'vcx> {
    fn from(s : &'vcx str) -> Self { Primitive::Str(Cow::Borrowed(s)) }
}

impl<'vcx> ConValue for Primitive<'vcx> {
    type TypeKind = PrimitiveType;
    type Context = StaticCtx;
    fn get_type_c(&self, _ctx : Self::Context) -> Self::TypeKind {
        match self {
            Primitive::Byte(b) => b.get_type_c(_ctx).into(),
            Primitive::Str(s) => s.get_type_c(_ctx).into()
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::typing::{StaticCtx, primitive::Primitive as PrimitiveType};
    use crate::value::Value;
    use super::Primitive;
    #[test]
    fn primitive_values_have_correct_type() {
        let vals : [Primitive; 2] = [
            33.into(),
            "hello".into()
        ];
        let tys : [PrimitiveType; 2] = [
            PrimitiveType::Byte,
            PrimitiveType::Str
        ];
        for (ty, rt) in tys.iter().zip(vals.iter()
            .map(|v| -> PrimitiveType { v.get_type(StaticCtx) })) {
            assert_eq!(*ty, rt);
        }
    }
}
