use owning_ref::OwningRef;

pub struct Eval<F, A, FO, AO> {
    function : OwningRef<FO, F>,
    arguments : OwningRef<AO, [A]>
}

impl<F, A, FO, AO> Eval<F, A, FO, AO> {
    #[inline]
    pub fn new<FT: Into<OwningRef<FO, F>>, AT: Into<OwningRef<AO, [A]>>>(f : FT, a : AT) -> Self {
        Eval { function : f.into(), arguments : a.into() }
    }
    #[inline] pub fn func(&self) -> &F { self.function.as_ref() }
    #[inline] pub fn args(&self) -> &[A] { self.arguments.as_ref() }
}

/// A function evaluation guaranteed to be complete
pub struct CompleteEval<F, A, FO, AO> { eval : Eval<F, A, FO, AO> }

/// A function evaluation guaranteed to be partial
pub struct PartialEval<F, A, FO, AO> { eval : Eval<F, A, FO, AO> }
