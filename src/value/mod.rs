use crate::typing::{Type, Context, Subcontext};

pub mod primitive;
pub mod eval;

/// The trait implemented by isotope values requiring a fixed context.
/// Automatically implements `Value`
pub trait ConValue {
    /// The context needed to interpret this value
    type Context : Context;
    /// The type of types this value can have
    type TypeKind : Type<Self::Context>;
    /// The type of this value
    fn get_type_c(&self, ctx : Self::Context) -> Self::TypeKind;
}

/// The trait implemented by all isotope values which can be interpreted in a given context
/// and have types of kind K
pub trait Value<C, K> where C: Context, K: Type<C> {
    /// The type of this value
    fn get_type(&self, ctx : C) -> K;
}

impl<C, K, V> Value<C, K> for V where
    V: ConValue,
    C: Context,
    V::Context : Subcontext<C>,
    V::TypeKind : Into<K>,
    K : Type<C> {
    #[inline]
    fn get_type(&self, ctx : C) -> K {
        self.get_type_c(V::Context::from_ctx(ctx)).into()
    }
}
